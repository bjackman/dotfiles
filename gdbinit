# Lets you have a .gdbinit in each project directory.
# Be careful, I guess.
set auto-load safe-path /

set prompt \033[1m \033[31mgdb $ \033[0m
set output-radix 0x10

set print pretty on

# source ~/dotfiles/colors.gdb

